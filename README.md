# Dynexitefiller

Note: Man muss anscheinend die Felder anklicken und die Tasten drücken, damit Dynexite die Antworten akzeptiert.

1. Im ersten Schritt berechnen wir die Eingabewerte.

2. Die Eingabewerte müssen nun der Variable `inputValues` zugewiesen werden. Der String sollte mit \` umschlossen werden.
   Deshalb müssen alle \` innerhalb der Eingabewerte mit einem \ escaped werden.

   Alle Textwerte müssen mit " umschlossen werden und Dezimalzahlen werden mit einem . anstatt einem , notiert.
   Falls die Eingabe in mehrere Zeilen verteilt werden soll, so können mehrere Strings mit einem + konkateniert werden.

3. Nun öffnen wir die Console des Browsers (im Regelfall sollte dies mit F12 möglich sein) und fügen die modifizierte Eingabe ein, bestätigen mit Enter woraufhin die Werte eingetragen sind.

```js
(() => {
  const inputValues =
    `"input 113", 112.34, 111, 110, 109, 108, 107, 106,` +
    ` 105, 104, 103, 102, 101, 100, 99, 98, 97, 96, 95, 94, 93, 92, 91, 90, 89, 88, 87, 86, 85, 84, 83, 82, 81, 80, 79, 78, 77, 76, 75, 74, 73, 72, 71, 70, 69, 68, 67, 66, 65, 64, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0`;

  const parsed = JSON.parse(`[${inputValues}]`);
  const inputList = document.querySelectorAll("input");

  if (parsed.length !== inputList.length) {
    console.log(
      `Die Anzahl der Inputfelder (${inputList.length}) unterscheidet sich von der Anzahl der Eingabewerte (${parsed.length})!`
    );
  }

  for (let i = 0; i < inputList.length; i++) {
    let value;
    switch (typeof parsed[i]) {
      case "number":
        value = parsed[i].toLocaleString("de-DE");
        break;
      default:
        value = parsed[i];
        break;
    }

    const item = inputList.item(i);
    item.value = value;
    item.classList.remove("ng-untouched");
    item.classList.add("ng-touched", "ng-dirty");
  }
})();
```

Alternativ kann auch folgende Zeile eingefügt werden, um die Inputfelder mit ihrem Index zu füllen.

```js
document.querySelectorAll("input").forEach((i, idx) => (i.value = idx + 1));
```

Getestet in Microsoft Edge, Google Chrome, Firefox.
